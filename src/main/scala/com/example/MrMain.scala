package com.example

/**
  * Created by websc on 31/05/2017.
  */
object MrMain {
  // If you don't extend App, then this value gets initialized correctly
  val num: Int = 123

  def getSth: Int = num

  def main(args: Array[String]):Unit = {
    println(s"num value is ${this.getSth}")
  }

}

