package com.example

/**
  * Created by websc on 31/05/2017.
  */
object MrLazy extends App {

  // If you lazily initialize this value, it works.
  // If you do not use "lazy", it seems to be initialized as zero in tests.
  lazy val num: Int = 123

  def getSth: Int = num

  println(s"num value is ${this.getSth}")

}

