package com.example


import org.scalatest._

class ScalaBugSpec extends FlatSpec with Matchers {

  "MrBuggy" should "should return numeric value of num" in {

    val expected = 123
    val results = MrBuggy.getSth

    results shouldBe expected
  }

  "MrLazy" should "should return numeric value of num" in {

    val expected = 123
    val results = MrLazy.getSth

    results shouldBe expected
  }

  "MrMain" should "should return numeric value of num" in {

    val expected = 123
    val results = MrMain.getSth

    results shouldBe expected
  }


  "MrNotSoAppy" should "should return numeric value of num" in {

    val expected = 123
    val results = MrNotSoAppy.getSth

    results shouldBe expected
  }
}
