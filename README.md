# Buggy behaviour in Scala App object #

* Stumbled across some odd behaviour in Scala the other day, so I thought I'd make a note of it here.

## The bug (or undocumented feature) ##

* Values seem to be initialised differently in a **runnable App object** or an **object that you have made runnable** by giving it a `main()` method.
* If you simply **call a method** on the object, **instead of running it**, you may see **different state** depending on whether it's an App or has an explicti `main()` method.
* When you hit this problem (as I did), it definitely feels like a **bug** in the Scala App object.
* But it could also be seen as a consequence of how App objects are initialised (or not) when you treat them as ordinary non-App objects.
* So maybe the obvious solution is not to use an App as if it were a non-App.
* Either way, here is the problem, and some simple work-arounds.

## Initialise value inside a Scala App object ##

* Here is a Scala object that extends App (so it will be runnable) and also initialises a numeric value to 123.
* It also has a method that does something with this value - in this case it simply returns it.

```
object MrBuggy extends App {

  val num: Int = 123

  def getSth: Int = num

  println(s"num value is ${this.getSth}")
}
```

### Run it ###

* `MrBuggy` extends `App` so you can run it from the command line e.g. via SBT.
* When you do this, it prints the expected message:

```
[info] Running com.example.MrBuggy 
num value is 123
[success] Total time: 3 s, completed 28-Jun-2017 13:54:38
```

* So far so good, right?
* But of course, we want to make sure the `getSth` method always works, so we need to write a test for it.

### Test it ###

* Here is a simple test of MrBuggy's `getSth` method:

```
  "MrBuggy" should "should return numeric value of num" in {

    val expected = 123
    val results = MrBuggy.getSth

    results shouldBe expected
  }
```

### Test results - not so good###

* The `num` value is initialised as zero here.

```
0 was not equal to 123
ScalaTestFailureLocation: com.example.ScalaBugSpec$$anonfun$1 at (com.example.ScalaBugSpec.scala:11)
Expected :123
Actual   :0
```

---

## Alternative: use a normal object with a main method ##

* We can change the buggy App by converting it to a normal object with a main method to make it runnable:

```
object MrMain { 

  val num: Int = 123

  def getSth: Int = num

  def main(args: Array[String]):Unit = {
    println(s"num value is ${this.getSth}")
  }
}
```

* When you run this object directly, the `num` value is initialised and displayed correctly.

```
[info] Running com.example.MrMain 
num value is 123
[success] Total time: 2 s, completed 28-Jun-2017 13:57:33

```
* When you run the tests, the object `num` value is still initialised correctly, and the test succeeds.

```
[info] MrMain
[info] - should should return numeric value of num
```

---

# Interpretation #

* If you do not extend App, then the object behaves as expected i.e. `num` is initialised correctly and the test succeeds.

## Initialization problem with App ##

* Seems there is a history here where people have tried something similar and hit similar problems.
* Here is [a StackOverflow discussion](https://stackoverflow.com/questions/20649982/understanding-delayedinit) of this kind of thing.
* App (or Application) is intended to make it easier to create a runnable object without having to explicitly write a `def main(args: Array[String]): Unit` method.
* In our examples, we are **not** running the object as an App, but simply treating it as a normal object and calling a method on it.
* But it looks like the object is only properly initialised if you use it as an App.

---

## Conclusions ##

* Generally, it seems you need to be careful how you use App if you need to initialise object state correctly.

### Option 1: don't use App trait ###

* You can **avoid using the App trait** and instead use a normal object with a `main` method as shown above.

### Option 2: be careful calling methods on App ###

* The problem here seems to be that calling the `getSth()` method on the App from an external (test) object doesn't work the same way it would work on a non-App object.
* It looks like the `val num` only gets initialised properly if you **run** the App.
* So you should **avoid calling methods on an App object if it is not running**.

### Option 3: lazily initialise the `val` in the App ###

* If you lazily initialise the value, it works as you would expect.

```
object MrLazy extends App {
  // If you lazily initialize this value, it works.
  // If you do not use "lazy", it seems to be initialized as zero in tests.
  lazy val num: Int = 123

  def getSth: Int = num

  println(s"num value is ${this.getSth}")
}
```
* However, as this behaviour was unexpected in the first place, it is probably better just to stick to an explicit `main()` method instead of using `App`.

### Option 4: don't use a runnable object if you don't need one! ###

* I stumbled into this error when I extended App for an object that I didn't actually need to run.
* I then spent rather too much time going around in circles trying to find out why my variable wasn't being initialised correctly, before I found a work-around (lazy initialisation).
* But in my case, the easiest solution would have been to simply not make the object runnable in the first place.
* Doh!

----