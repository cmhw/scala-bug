package com.example

/**
  * Created by websc on 31/05/2017.
  */
object MrNotSoAppy {
  // If you don't extend App, then this value gets initialized correctly

  val num: Int = 123

  def getSth: Int = num


  println(s"num value is ${this.getSth}")

}

