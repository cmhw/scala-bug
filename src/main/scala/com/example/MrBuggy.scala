package com.example

object MrBuggy extends App {

  val num: Int = 123

  def getSth: Int = num

  println(s"num value is ${this.getSth}")

}
